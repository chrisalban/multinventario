<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

//Rutas para los equipos
Route::resource('hardware', 'Web\HardwareController')->only(['index', 'store', 'update']);
Route::post('hardware/validation', 'Web\HardwareController@validation')->name('hardware.validation'); //Ruta al metodo que valida los datos cargados antes de subirlos
Route::post('hardware/upload', 'Web\HardwareController@upload')->name('hardware.upload'); //Ruta al metodo que carga los datos validados
Route::get('hardware/export/', 'Web\HardwareController@export');

//Rutas para los usuarios
Route::resource('users', 'Web\UserController')->only(['index', 'store', 'update']);
Route::post('users/validation', 'Web\UserController@validation')->name('users.validation'); //Ruta al metodo que valida los datos cargados antes de subirlos
Route::post('users/upload', 'Web\UserController@upload')->name('users.upload'); //Ruta al metodo que carga los datos validados

//Rutas para los usuarios
Route::resource('events', 'Web\EventController')->only(['index', 'store', 'update']);
Route::post('events/validation', 'Web\EventController@validation')->name('events.validation'); //Ruta al metodo que valida los datos cargados antes de subirlos
Route::post('events/upload', 'Web\EventController@upload')->name('events.upload'); //Ruta al metodo que carga los datos validados

//Rutas para las categorias
Route::resource('categories', 'Web\CategoryController')->only(['index', 'store', 'update']);
//Rutas para los proveedores
Route::resource('providers', 'Web\ProviderController')->only(['index', 'store', 'update']);
//Rutas para las unidades
Route::resource('units', 'Web\UnitController')->only(['index', 'store', 'update']);
//Rutas para los cargos
Route::resource('charges', 'Web\ChargeController')->only(['index', 'store', 'update']);
