<?php

namespace multinventario\Http\Controllers\Web;

use Illuminate\Http\Request;
use multinventario\Http\Controllers\Controller;
use multinventario\Event;
use multinventario\User;
use multinventario\Hardware;
use multinventario\Http\Requests\EventRequest;

class EventController extends Controller
{
  public function index(Request $request){
    if($request->get('pagination') !== 'none'){
      $event = Event::join('users as user', 'events.user_id', '=', 'user.id')
      ->join('hardware as hardware', 'events.hardware_id', '=', 'hardware.id')
      ->select('events.*', 'hardware.label', 'user.name')
      ->with('hardware', 'user')
      ->orderBy($request->get('sortBy'), $request->get('sortDesc'))
      ->filter($request->get('filter'))
      ->paginate(10);
      return $event;
    }
    $event = Event::orderBy('date', 'DESC')->with('hardware', 'user')->get();
    return $event;
  }

  public function store(EventRequest $request) {
    Event::create($request->all());
    return;
  }

  public function update(EventRequest $request, $id) {
    Event::find($id)->update($request->all());
    return;
  }

  //Metodo que hace la verificación de los datos importados
  public function check(Request $request) {
    $events = $request -> all();
    $upload = true;
    $line = 1;
    $mistakes = [];
    foreach ($events['import'] as $item) {
      if(Hardware::where('label',$item[0])->get()->count() !== 0){
        array_push($mistakes, [
          'line' => $line,
          'message' => "Elemento con etiqueta '$item[0]' ya registrado"
        ]);
        $upload = false;
      };
      if(Category::where('name',$item[1])->get()->count() === 0){
        array_push($mistakes, [
          'line' => $line,
          'message' => "No existe una categoria con el nombre '$item[1]'"
        ]);
        $upload = false;
      };
      if($item[2] !== 'N/A' && Provider::where('name',$item[2])->get()->count() === 0){
        array_push($mistakes, [
          'line' => $line,
          'message' => "No existe un proveedor con el nombre '$item[2]'"
        ]);
        $upload = false;
      };
      if($item[7] !== 'N/A' && filter_var($item[7], FILTER_VALIDATE_IP) === false){
        array_push($mistakes, [
          'line' => $line,
          'message' => "La IP '$item[7]' ingresada no es una IP válida"
        ]);
        $upload = false;
      };
      if($item[9] !== 'N/A' && !ctype_digit($item[9])){
        array_push($mistakes, [
          'line' => $line,
          'message' => "El tamaño de disco ingresado no es un valor valido en GB".ctype_digit($item[9])
        ]);
        $upload = false;
      };
      if($item[10] !== 'N/A' && !is_numeric($item[10]) && !is_float((float)$item[10])){
        array_push($mistakes, [
          'line' => $line,
          'message' => "El tamaño de memoria ingresado no es un valor valido en GB"
        ]);
        $upload = false;
      };
      $item[12] = str_replace('$', '', $item[12]);
      if($item[12] === 'N/A' || (!ctype_digit($item[12]))){
        array_push($mistakes, [
          'line' => $line,
          'message' => "El costo ingresado no es un valor valido entero"
        ]);
        $upload = false;
      };
      if(strcasecmp($item[15], 'si') !== 0 && strcasecmp($item[15], 'no') !== 0){
        array_push($mistakes, [
          'line' => $line,
          'message' => "El valor de la licencia de Office debe ser SI o NO"
        ]);
        $upload = false;
      };
      if(strcasecmp($item[16], 'si') !== 0 && strcasecmp($item[16], 'no') !== 0){
        array_push($mistakes, [
          'line' => $line,
          'message' => "El valor de la licencia de Windows debe ser SI o NO"
        ]);
        $upload = false;
      };
      if(strcasecmp($item[17], 'si') !== 0 && strcasecmp($item[17], 'no') !== 0){
        array_push($mistakes, [
          'line' => $line,
          'message' => "El valor de siniestro debe ser SI o NO "
        ]);
        $upload = false;
      };
      $line++;
    }
    return [
      'mistakes' => $mistakes,
      'upload' => $upload
    ];
  }

  //Metodo que sube los datos verificados
  public function upload(Request $request) {
    $hardware = $request -> all();
    foreach ($hardware['import'] as $item) {
      $createItem = new Hardware;
      $createItem->label = $item[0];
      $category = Category::where('name',$item[1])->first();
      $createItem->category_id = $category->id;
      $provider = Provider::where('name',$item[2])->first();
      if($provider !== null) $createItem->provider_id = $provider->id;
      if(strcasecmp($item[3], 'n/a') !== 0) $createItem->hostname = $item[3];
      if(strcasecmp($item[4], 'n/a') !== 0) $createItem->brand = $item[4];
      if(strcasecmp($item[5], 'n/a') !== 0) $createItem->model = $item[5];
      if(strcasecmp($item[6], 'n/a') !== 0) $createItem->serial = $item[6];
      if(strcasecmp($item[7], 'n/a') !== 0) $createItem->ip = $item[7];
      if(strcasecmp($item[8], 'n/a') !== 0) $createItem->processor = $item[8];
      if(strcasecmp($item[9], 'n/a') !== 0) $createItem->disc = $item[9];
      if(strcasecmp($item[10], 'n/a') !== 0) $createItem->ram = str_replace(',', '.', $item[10]);
      if(strcasecmp($item[11], 'n/a') !== 0) $createItem->so = $item[11];
      $createItem->cost = str_replace(',','.',str_replace('$', '', $item[12]));
      if(strcasecmp($item[13], 'n/a') !== 0)  $createItem->anydesk_hardware = $item[13];
      if(strcasecmp($item[14], 'n/a') !== 0) $createItem->anydesk_password = $item[14];
      if(strcasecmp($item[15], 'si') !== 0) $item[15] = true;
      else $item[15] = false;
      $createItem->office_license = $item[15];
      if(strcasecmp($item[16], 'si') !== 0) $item[16] = true;
      else $item[16] = false;
      $createItem->windows_license = $item[16];
      if(strcasecmp($item[17], 'si') !== 0) $item[17] = true;
      else $item[17] = false;
      $createItem->sinister = $item[17];

      $createItem->save();
    };
    return;
  }
}
