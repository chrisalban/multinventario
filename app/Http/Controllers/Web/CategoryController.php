<?php

namespace multinventario\Http\Controllers\Web;

use Illuminate\Http\Request;
use multinventario\Http\Controllers\Controller;
use multinventario\Http\Requests\CategoryRequest;
use multinventario\Category;

class CategoryController extends Controller
{
  public function index(Request $request){
    if($request->get('pagination') !== 'none'){
      $category = Category::orderBy($request->get('sortBy'), $request->get('sortDesc'))
      ->filter($request->get('filter'))
      ->paginate(10);
      return $category;
    }
    $category = Category::orderBy('name', 'ASC')->get();
    return $category;
  }

  public function store(CategoryRequest $request) {
    Category::create($request->all());
    return;
  }

  public function update(Request $request, $id) {
    Category::find($id)
      ->update($request->all());
    return;
  }
}
