<?php

namespace multinventario\Http\Controllers\Web;

use Illuminate\Http\Request;
use multinventario\Http\Controllers\Controller;
use multinventario\Http\Requests\ChargeRequest;
use multinventario\Charge;

class ChargeController extends Controller
{
    public function index(Request $request){
      if($request->get('pagination') !== 'none'){
        $charge = Charge::orderBy($request->get('sortBy'), $request->get('sortDesc'))
        ->filter($request->get('filter'))
        ->paginate(10);
        return $charge;
      }
      $charge = Charge::orderBy('name', 'ASC')->get();
      return $charge;
    }

    public function store(ChargeRequest $request) {
      Charge::create($request->all());
      return;
    }

    public function update(Request $request, $id) {
      Charge::find($id)
        ->update($request->all());
      return;
    }
}
