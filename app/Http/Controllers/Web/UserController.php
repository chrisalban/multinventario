<?php

namespace multinventario\Http\Controllers\Web;

use Illuminate\Http\Request;
use multinventario\Http\Controllers\Controller;
use multinventario\User;
use multinventario\Unit;
use multinventario\Charge;
use multinventario\Http\Requests\UserRequest;
use Illuminate\Support\Facades\Validator;

class UserController extends Controller
{
  public function index(Request $request){
    if($request->get('pagination') !== 'none'){
      $user = User::join('units as unit', 'users.unit_id', '=', 'unit.id')
      ->select('users.*')
      ->with('unit', 'charge')
      ->orderBy($request->get('sortBy'), $request->get('sortDesc'))
      ->filter($request->get('filter'))
      ->paginate(10);
      return $user;
    }
    $user = User::orderBy('name', 'ASC')->get();
    return $user;
  }

  public function store(UserRequest $request) {
    User::create($request->all());
    return;
  }

  public function update(UserRequest $request, $id) {
    User::find($id)->update($request->all());
    return;
  }

  //Metodo que hace la verificación de los datos importados
  public function validation(Request $request) {
    $users = $request -> all();
    $upload = true;
    $line = 0;
    $mistakes = [];
    $duplicates = [];

    foreach ($users['import'] as $user) {
      array_push($duplicates, $user['code']);
      $validator = Validator::make($user, [
          'code'        => 'required|unique:users,code|max:10',
          'name'        => 'required',
          'lastname'    => 'required',
          'unit'        => 'required|exists:units,name',
          'charge'      => 'required|exists:charges,name',
          'email'       => 'nullable|email',
          'forwarding'  => 'nullable|email',
          'pc_user'     => 'required|max:50',
          'pc_password' => 'required',
          'active'      => 'required|boolean',
      ],[
        'exists'  => "No existe una :attribute con el nombre ':input'",
        'unique'  => "Ya existe una :attribute con el nombre ':input'",
        'ip'      => "La dirección :attribute: ':input'",
        'numeric' => "El valor de :attribute debe ser un numero: ':input'",
        'boolean' => "El valor de :attribute debe ser si o no: ':input'",
        'email'   => "El :attribute debe ser una dirección de correo: ':input'",
        'max'     => "El campor :attribute debe tener un maximo de :max caracteres: ':input'"
      ],[
        'code'        => 'cedula',
        'name'        => 'nombre',
        'lastname'    => 'apellido',
        'unit'        => 'unidad',
        'charge'      => 'cargo',
        'email'       => 'correo',
        'forwarding'  => 'reenvio',
        'active'      => 'activo',
        'pc_user'     => 'usuario pc',
        'pc_password'     => 'clave pc'
      ]);

      if ($validator->fails()) {
          $upload = false;
          array_push($mistakes, [
            'line' => $line,
            'messages' => $validator->errors()->all()
          ]);
      }
      $line++;
    }

    $duplicates = array_count_values($duplicates);
    foreach ($duplicates as $code => $count) {
      if ($count === 1) unset($duplicates[$code]);
      else $upload = false;
    }
    return [
      'duplicates'  => $duplicates,
      'mistakes'    => $mistakes,
      'upload'      => $upload
    ];
  }

  //Metodo que sube los datos verificados
  public function upload(Request $request) {
    $users = $request -> all();
    foreach ($users['import'] as $user) {
      $unit = Unit::where('name',$user['unit'])->first();
      $user['unit_id'] = $unit->id;
      $charge = Charge::where('name',$user['charge'])->first();
      $user['charge_id'] = $charge->id;
      User::create($user);
    };
    return;
  }
}
