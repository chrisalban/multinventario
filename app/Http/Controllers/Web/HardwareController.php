<?php

namespace multinventario\Http\Controllers\Web;

use Illuminate\Http\Request;
use multinventario\Http\Controllers\Controller;
use multinventario\Hardware;
use multinventario\Category;
use multinventario\Provider;
use multinventario\Http\Requests\HardwareRequest;
use Illuminate\Support\Facades\Validator;
use multinventario\Exports\HardwareExport;
use Maatwebsite\Excel\Facades\Excel;

class HardwareController extends Controller
{
  public function index(Request $request){
    //return $request->get('filter');
    if($request->get('pagination') !== 'none'){
      $hardware = Hardware::join('categories as category', 'hardware.category_id', '=', 'category.id')
      ->select('hardware.*')
      ->with('category', 'provider')
      ->orderBy($request->get('sortBy'), $request->get('sortDesc'))
      ->filter($request->get('filter'))
      ->paginate(10);
      return $hardware;
    }
    $hardware = Hardware::orderBy('label', 'ASC')->get();
    return $hardware;
  }

  public function store(HardwareRequest $request) {
    Hardware::create($request->all());
    return;
  }

  public function update(HardwareRequest $request, $id) {
    Hardware::find($id)->update($request->all());
    return;
  }

  //Metodo que hace la verificación de los datos importados
  public function validation(Request $request) {
    $hardware = $request -> all();
    $upload = true;
    $line = 0;
    $mistakes = [];
    $duplicates = [];

    foreach ($hardware['import'] as $hard) {
      array_push($duplicates, $hard['label']);
      $validator = Validator::make($hard, [
          'label'           => 'required|unique:hardware,label',
          'category'        => 'exists:categories,name',
          'provider'        => 'nullable|exists:providers,name',
          'model'           => 'nullable|max:70',
          'serial'          => 'nullable|max:190',
          'ip'              => 'nullable|ip',
          'disc'            => 'nullable|numeric',
          'ram'             => 'nullable|numeric',
          'cost'            => 'nullable|numeric',
          'office_license'  => 'boolean',
          'windows_license' => 'boolean',
          'status'        => 'required|in:available,unavailable,sinister'
      ],[
        'exists'    => "No existe una :attribute con el nombre ':input'",
        'unique'    => "Ya existe una :attribute con el nombre ':input'",
        'ip'        => "La dirección :attribute: ':input'",
        'numeric'   => "El valor de :attribute debe ser un numero: ':input'",
        'boolean'   => "El valor de :attribute debe ser si o no: ':input'",
        'in'   => "El valor de :attribute debe ser available, unavailable o sinister: ':input'",
      ],[
        'label'             => 'etiqueta',
        'category'          => 'categoria',
        'provider'          => 'proveedor',
        'disc'              => 'almacenamiento',
        'ram'               => 'memoria',
        'office_license'    => 'licencia de office',
        'windows_license'   => 'licencia de windows',
        'status'          => 'estado'
      ]);

      if ($validator->fails()) {
          $upload = false;
          array_push($mistakes, [
            'line' => $line,
            'messages' => $validator->errors()->all()
          ]);
      }
      $line++;
    }

    $duplicates = array_count_values($duplicates);
    foreach ($duplicates as $code => $count) {
      if ($count === 1) unset($duplicates[$code]);
      else $upload = false;
    }

    return [
      'duplicates'  => $duplicates,
      'mistakes' => $mistakes,
      'upload' => $upload
    ];
  }

  //Metodo que sube los datos verificados
  public function upload(Request $request) {
    $hardware = $request -> all();
    foreach ($hardware['import'] as $hard) {
      $category = Category::where('name',$hard['category'])->first();
      $hard['category_id'] = $category->id;
      $provider = Provider::where('name',$hard['provider'])->first();
      if($provider !== null) $hard['provider_id'] = $provider->id;
      Hardware::create($hard);
    };
    return;
  }

  public function export(Request $request) {
      return Excel::download(new HardwareExport($request->get('filter')), 'hardware.xlsx');
  }
}
