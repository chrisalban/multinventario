<?php

namespace multinventario\Http\Controllers\Web;

use Illuminate\Http\Request;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Facades\DB;
use multinventario\Http\Controllers\Controller;
use multinventario\User;
use multinventario\Hardware;
use multinventario\Event;

class DashboardController extends Controller
{
  public function getUsersHardware() {

    $activeUsers = Event::select('user_id')
      ->whereHas('user', function(Builder $query){
          $query->where('active', '=', true);
        })
      ->where('event', '=', 0)
      ->groupBy('user_id')
      ->get();


    $events = [];

    foreach ($activeUsers as $activeUser) {

      $response = Event::where('user_id', '=', $activeUser->user_id)
        ->latest()
        ->first();


      $user = User::with('unit', 'charge')
        ->find($response->user_id);
      $response->user = $user;

      $hardware = Hardware::with('category', 'provider')
        ->find($response->hardware_id);
      $response->hardware = $hardware;

      if($response !== null)
      array_push($events, $response);
    }

    return $events;
  }
}
