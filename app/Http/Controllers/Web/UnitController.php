<?php

namespace multinventario\Http\Controllers\Web;

use Illuminate\Http\Request;
use multinventario\Http\Controllers\Controller;
use multinventario\Http\Requests\UnitRequest;
use multinventario\Unit;
use Illuminate\Support\Facades\Validator;

class UnitController extends Controller
{
    public function index(Request $request){
      if($request->get('pagination') !== 'none'){
        $unit = Unit::orderBy($request->get('sortBy'), $request->get('sortDesc'))
        ->filter($request->get('filter'))
        ->paginate(10);
        return $unit;
      }
      $unit = Unit::orderBy('name', 'ASC')->get();
      return $unit;
    }

    public function store(UnitRequest $request) {
      Unit::create($request->all());
      return;
    }

    public function update(UnitRequest $request, $id) {
      Unit::find($id)
        ->update($request->all());
      return;
    }
}
