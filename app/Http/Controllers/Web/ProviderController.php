<?php

namespace multinventario\Http\Controllers\Web;

use Illuminate\Http\Request;
use multinventario\Http\Controllers\Controller;
use multinventario\Http\Requests\ProviderRequest;
use multinventario\Provider;

class ProviderController extends Controller
{
  public function index(Request $request){
    if($request->get('pagination') !== 'none'){
      $provider = Provider::orderBy($request->get('sortBy'), $request->get('sortDesc'))
      ->filter($request->get('filter'))
      ->paginate(10);
      return $provider;
    }
    $provider = Provider::orderBy('name', 'ASC')->get();
    return $provider;
  }

  public function store(ProviderRequest $request) {
    Provider::create($request->all());
    return;
  }

  public function update(ProviderRequest $request, $id) {
    Provider::find($id)->update($request->all());
    return;
  }
}
