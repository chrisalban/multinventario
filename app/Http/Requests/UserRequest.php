<?php

namespace multinventario\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class UserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
          'code' => 'required',
          'name' => 'required',
          'lastname' => 'required',
          'unit_id' => 'required',
          'charge_id' => 'required',
          'pc_user' => "required",
          'pc_password' => 'required',
        ];
    }

    public function attributes(){
      return [
        'code' => 'cedula',
        'lastname' => 'apellido',
        'unit_id' => 'unidad',
        'charge_id' => 'cargo',
        'pc_user' => 'usuario Pc',
        'pc_password' => 'clave Pc',
      ];
    }
}
