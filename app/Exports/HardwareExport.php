<?php

namespace multinventario\Exports;

use multinventario\Hardware;
use multinventario\Exports\Hardware\HardwareSheet;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\WithMultipleSheets;

class HardwareExport implements ShouldAutoSize, WithMultipleSheets
{

    public function __construct($filter) {
        $this->filter = $filter;
    }

    public function sheets(): array {
      $sheets=[];

      $hardwareProviders = Hardware::select('provider_id')
        ->filter($this->filter)
        ->groupBy('provider_id')
        ->get();

      foreach ($hardwareProviders as $provider) {
        $sheets[] = new HardwareSheet($provider->provider_id);
      }

      return $sheets;
    }

}
