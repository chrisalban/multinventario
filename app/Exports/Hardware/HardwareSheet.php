<?php
namespace multinventario\Exports\Hardware;

use multinventario\Hardware;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\WithTitle;
use Maatwebsite\Excel\Concerns\FromView;

/**
 *
 */
class HardwareSheet implements WithTitle, FromView
{

  public function __construct($provider_id) {
      $this->hardwareCollection = Hardware::where('provider_id', '=', $provider_id)
        ->with('category', 'provider')
        ->get();
  }

  public function view(): View {
      return view('exports.hardware', [
          'hardwareCollection' => $this->hardwareCollection
      ]);
  }

  public function title(): string {
      $title = ($this->hardwareCollection[0]->provider) ? $this->hardwareCollection[0]->provider->name : 'Multiapoyo' ;
      return $title;
  }

}


?>
