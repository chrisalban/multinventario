<?php

namespace multinventario;

use Illuminate\Database\Eloquent\Model;

class Event extends Model
{
  protected $fillable = [
      'hardware_id', 'user_id', 'event', 'diagnostic', 'repair', 'details'
  ];

  public function user(){
    return $this->belongsTo(User::class);
  }

  public function hardware(){
    return $this->belongsTo(Hardware::class);
  }

  //Query Scope

  public function scopeFilter($query, $filter){
    if ($filter !== 'null'){
      return $query
        ->where('user.name', 'LIKE', "%$filter%")
        ->orWhere('hardware.label', 'LIKE', "%$filter%")
        ->orWhere('event', 'LIKE', "%$filter%");
    }
  }
}
