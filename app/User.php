<?php

namespace multinventario;

use Illuminate\Database\Eloquent\Model;

class User extends Model
{
  protected $fillable = [
      'code', 'name' , 'lastname', 'unit_id', 'charge_id', 'email', 'password', 'forwarding', 'mega_recovery', 'pc_user', 'pc_password', 'active',
  ];

  public function unit(){
    return $this->belongsTo(Unit::class);
  }

  public function charge(){
    return $this->belongsTo(Charge::class);
  }

  public function events(){
    return $this->hasMany(Event::class);
  }

  public function scopeFilter($query, $filter){
    if ($filter !== 'null'){
      return $query
        ->where('code', 'LIKE', "%$filter%")
        ->orWhere('users.name', 'LIKE', "%$filter%")
        ->orWhere('users.lastname', 'LIKE', "%$filter%")
        ->orWhere('unit.name', 'LIKE', "%$filter%")
        ->orWhere('email', 'LIKE', "%$filter%")
        ->orWhere('password', 'LIKE', "%$filter%");
    }
  }
}
