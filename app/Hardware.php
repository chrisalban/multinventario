<?php

namespace multinventario;

use Illuminate\Database\Eloquent\Model;

class Hardware extends Model
{
  protected $fillable = [
      'label', 'category_id', 'provider_id', 'hostname', 'brand', 'model', 'serial', 'ip', 'processor', 'disc', 'ram', 'so', 'cost', 'anydesk_user', 'anydesk_password', 'office_license', 'windows_license', 'status',
  ];

  public function category(){
    return $this->belongsTo(Category::class);
  }

  public function provider(){
    return $this->belongsTo(Provider::class);
  }

  public function events(){
    return $this->hasMany(Event::class);
  }

  //Query Scope

  public function scopeFilter($query, $filter){
    if ($filter !== 'null'){
      return $query
        ->where('label', 'LIKE', "%$filter%")
        ->orWhere('category.name', 'LIKE', "%$filter%")
        ->orWhere('hostname', 'LIKE', "%$filter%")
        ->orWhere('brand', 'LIKE', "%$filter%")
        ->orWhere('model', 'LIKE', "%$filter%")
        ->orWhere('ip', 'LIKE', "%$filter%")
        ->orWhere('status', 'LIKE', "%$filter%");
    }
  }
}
