<?php

namespace multinventario;

use Illuminate\Database\Eloquent\Model;

class Unit extends Model
{
  protected $fillable = [
      'name'
  ];

  public function users(){
    return $this->hasMany(User::class);
  }

  public function scopeFilter($query, $name){
    if ($name !== 'null'){
      return $query
        ->where('name', 'LIKE', "%$name%");
    }
  }
}
