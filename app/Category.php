<?php

namespace multinventario;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
  protected $fillable = [
      'name',
  ];

  public function hardware(){
    return $this->hasMany(Hardware::class);
  }

  public function scopeFilter($query, $name){
    if ($name !== 'null'){
      return $query
        ->where('name', 'LIKE', "%$name%");
    }
  }
}
