<?php

namespace multinventario;

use Illuminate\Database\Eloquent\Model;

class Provider extends Model
{
  protected $fillable = [
      'name', 'email', 'phone',
  ];

  public function hardware(){
    return $this->hasMany(Hardware::class);
  }

  public function scopeFilter($query, $name){
    if ($name !== 'null'){
      return $query
        ->where('name', 'LIKE', "%$name%")
        ->orWhere('email', 'LIKE', "%$name%")
        ->orWhere('phone', 'LIKE', "%$name%");
    }
  }
}
