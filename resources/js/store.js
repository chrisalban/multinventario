import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    pageIndex: '',
    filter: {
      keyword: '',

    }
  },
  mutations: {
    setPageIndex(state, index){
      state.pageIndex = index
    },
    setItem(state, item) {
      state.intem = item
    }
  }
})
