import Vue from 'vue'
import VueRouter from 'vue-router'

Vue.use(VueRouter)

export default new VueRouter({
  base: 'multinventario/public',
  //mode: 'history',
  routes: [
    {
      path: '/',
      redirect: '/dashboard'
    },
    {
      path: '/dashboard',
      name: 'dashboard',
      component: require('./views/Dashboard').default
    },
    {
      path: '/hardware',
      name: 'hardware',
      component: require('./views/Hardware').default
    },
    {
      path: '/users',
      name: 'users',
      component: require('./views/Users').default
    },
    {
      path: '/events',
      name: 'events',
      component: require('./views/Events').default
    },
    {
      path: '/units',
      name: 'units',
      component: require('./views/Units').default
    },
    {
      path: '/charges',
      name: 'charges',
      component: require('./views/Charges').default
    },
    {
      path: '/categories',
      name: 'categories',
      component: require('./views/Categories').default
    },
    {
      path: '/providers',
      name: 'providers',
      component: require('./views/Providers').default
    },
    {
      path: '*',
      name: '404',
      component: require('./views/404').default
    }
  ]
})
