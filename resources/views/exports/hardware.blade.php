<table>
  <thead>
    <tr>
      <th>Codigo</th>
      <th>Categoria</th>
      <th>Marca</th>
      <th>Modelo</th>
      <th>Serie</th>
      <th>Sistema Operativo</th>
      <th>Procesador</th>
      <th>Almacenamiento</th>
      <th>Ram</th>
      <th>Valor</th>
    </tr>
  </thead>
  <tbody>
    @foreach($hardwareCollection as $hardware)
      <tr>
        <td>{{ $hardware->label }}</td>
        <td>{{ $hardware->category->name }}</td>
        <td>{{ $hardware->brand }}</td>
        <td>{{ $hardware->model }}</td>
        <td>{{ $hardware->serial }}</td>
        <td>{{ $hardware->so }}</td>
        <td>{{ $hardware->processor }}</td>
        <td>{{ $hardware->disc}}</td>
        <td>{{ $hardware->ram }}</td>
        <td>{{ $hardware->cost }}</td>
      </tr>
    @endforeach
  </tbody>
</table>
