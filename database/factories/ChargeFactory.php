<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use multinventario\Charge;
use Faker\Generator as Faker;

$factory->define(Charge::class, function (Faker $faker) {
    return [
        'name' => $faker->text(20)
    ];
});
