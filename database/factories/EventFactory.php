<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use multinventario\Event;
use Faker\Generator as Faker;

$factory->define(Event::class, function (Faker $faker) {
    return [
      'hardware_id' => rand(1, 300),
      'user_id' => rand(1, 100),
      'event' => $faker->randomElement([ 0, 1, 2 ]),
      'diagnostic' => $faker->sentence(30),
      'repair' => $faker->sentence(30),
      'details' => $faker->sentence(30)
    ];
});
