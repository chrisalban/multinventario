<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use multinventario\Provider;
use Faker\Generator as Faker;

$factory->define(Provider::class, function (Faker $faker) {
    return [
      'name' => $faker->text(10),
      'email' => $faker->optional()->safeEmail,
      'phone' => $faker->optional()->phoneNumber
    ];
});
