<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use multinventario\Hardware;
use Faker\Generator as Faker;

$factory->define(Hardware::class, function (Faker $faker) {
    do
        $label = $faker->randomElement(['LPT', 'CPU', 'TEC', 'MON']) . '-' . rand(100,999);
    while (Hardware::where('label', $label)->exists());

    return [
      'label' => $label,
      'category_id' => rand(1, 20),
      'provider_id' => rand(1, 20),
      'hostname' =>  $faker->optional()->randomElement(['LPT', 'CPU', 'TEC', 'MON', 'PERSONAL']),
      'brand' => $faker->text(10),
      'model' => $faker->unique()->text(10),
      'serial' => $faker->unique()->text(30),
      'ip' => $faker->unique()->ipv4,
      'processor' => $faker->optional()->randomElement(['i3', 'i5', 'i7', 'dual core', 'amd']),
      'disc' => $faker->optional()->randomNumber($nbDigits = 3),
      'ram' => $faker->optional()->randomNumber($nbDigits = 2),
      'so' => $faker->optional()->randomElement(['Windows 7x32', 'Windows 7x64', 'Windows 10x32', 'Windows 10x64']),
      'cost' => $faker->randomFloat($nbMaxDecimals = 2, $min = 100, $max = 800),
      'office_license' => $faker->optional()->boolean($chanceOfGettingTrue = 50),
      'windows_license' => $faker->optional()->boolean($chanceOfGettingTrue = 50),
      'status' => $faker->randomElement([ 'sinister', 'available', 'unavailable' ]),
    ];
});
