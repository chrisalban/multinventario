<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use multinventario\User;
use Faker\Generator as Faker;

$factory->define(User::class, function (Faker $faker) {
    $code = '';
    for ($i = 0; $i<10; $i++)
    {
      $code .= rand(0,9);
    }
    return [
      'code' => $code,
      'name' => $faker->name,
      'lastname' => $faker->lastname,
      'unit_id' => rand(1,10),
      'charge_id' => rand(1,20),
      'email' => $faker->unique()->safeEmail,
      'password' => $faker->regexify('[A-Za-zA-Za-z]*[A-Z0-9][0-90-90-90-9]'),
      'forwarding' => $faker->unique()->safeEmail,
      'mega_recovery' => $faker->text(30),
      'pc_user' => $faker->unique()->text(50),
      'pc_password' => 'Ejecutivo12',
      'active' => $faker->boolean($chanceOfGettingTrue = 50)
    ];
});
