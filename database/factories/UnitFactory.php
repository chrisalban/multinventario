<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use multinventario\Unit;
use Faker\Generator as Faker;

$factory->define(Unit::class, function (Faker $faker) {
    return [
        'name' => $faker->text(10)
    ];
});
