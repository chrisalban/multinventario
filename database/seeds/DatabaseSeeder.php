<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call([
          CategoriesTableSeeder::class,
          ChargesTableSeeder::class,
          ProvidersTableSeeder::class,
          UnitsTableSeeder::class,

          //Seeder con relaciones
          UsersTableSeeder::class,
          HardwareTableSeeder::class,
          EventsTableSeeder::class
        ]);
    }
}
