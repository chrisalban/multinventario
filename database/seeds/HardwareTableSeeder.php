<?php

use multinventario\Hardware;
use Illuminate\Database\Seeder;

class HardwareTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        for($count = 1; $count <= 300; $count++)
        {
            $hardware = factory(Hardware::class)->create();

            $hardware->category()->associate(rand(1, 20));
            $hardware->provider()->associate(rand(1, 20));
        }
        
    }
}
