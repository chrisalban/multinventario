<?php

use multinventario\Event;
use Illuminate\Database\Seeder;

class EventsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(Event::class, 200)->create()->each(function(Event $event){
            $event->user()->associate(rand(1, 100));
            $event->hardware()->associate(rand(1, 300));
          });
    }
}
