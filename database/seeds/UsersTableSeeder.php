<?php

use multinventario\User;
use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(User::class, 100)->create()->each(function(User $user){
            $user->unit()->associate(rand(1, 10));
            $user->charge()->associate(rand(1, 20));
          });
    }
}
