<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateHardwareTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('hardware', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('label', 10)->unique();
            $table->bigInteger('category_id')->unsigned();
            $table->bigInteger('provider_id')->unsigned()->nullable();
            $table->string('hostname')->nullable();
            $table->string('brand')->nullable();
            $table->string('model', 70)->nullable();
            $table->string('serial', 190)->nullable();
            $table->string('ip', 16)->nullable();
            $table->string('processor')->nullable();
            $table->integer('disc')->nullable();
            $table->double('ram')->nullable();
            $table->string('so')->nullable();
            $table->double('cost');
            $table->string('anydesk_user')->nullable();
            $table->string('anydesk_password')->nullable();
            $table->boolean('office_license')->nullable();
            $table->boolean('windows_license')->nullable();
            $table->boolean('sinister')->default(false);
            $table->timestamps();

            //Relations
            $table->foreign('provider_id')->references('id')->on('providers')
              //->onDelete('cascade')
              ->onUpdate('cascade');
            $table->foreign('category_id')->references('id')->on('categories')
              //->onDelete('cascade')
              ->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('hardware');
    }
}
