<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('code', 10)->unique();
            $table->string('name');
            $table->string('lastname');
            $table->bigInteger('unit_id')->unsigned();
            $table->bigInteger('charge_id')->unsigned();
            $table->string('email')->nullable();
            $table->string('password')->nullable();
            $table->string('forwarding')->nullable();
            $table->string('mega_recovery')->nullable();
            $table->string('pc_user', 50);
            $table->string('pc_password');
            $table->boolean('active')->default(false);
            $table->timestamps();

            //Relations
            $table->foreign('unit_id')->references('id')->on('units')
              //->onDelete('cascade')
              ->onUpdate('cascade');

            $table->foreign('charge_id')->references('id')->on('charges')
              //->onDelete('cascade')
              ->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
