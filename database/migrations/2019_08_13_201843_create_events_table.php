<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEventsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('events', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('hardware_id')->unsigned();
            $table->bigInteger('user_id')->unsigned();
            $table->date('date');
            $table->tinyInteger('event');
            $table->longText('diagnostic')->nullable();
            $table->longText('repair')->nullable();
            $table->longText('details')->nullable();
            $table->timestamps();

            //Relations
            $table->foreign('hardware_id')->references('id')->on('hardware')
              //->onDelete('cascade')
              ->onUpdate('cascade');
            $table->foreign('user_id')->references('id')->on('users')
              //->onDelete('cascade')
              ->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('events');
    }
}
